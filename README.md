# Ancient Nightmares - Untold Horrors

GM-Less story game for 2 to 4 players based on H.P. Lovecraft Cthulhu Mythos.

If you play it or have suggestions, corrections, please leave them as issues. I'm
very interested in any feedback you can give.
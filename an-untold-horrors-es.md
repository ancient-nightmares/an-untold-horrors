# Ancient Nightmares - Untold Horrors

Juego de rol narrativo basado en los Mitos de Cthulhu de H.P. Lovecraft

v0.2a @2019 Oliver Gutiérrez Suárez

Licensed under [Creative Commons CC-BY-NC-SA 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/)

## Introducción

Este juego está pensado para entre 1 y 4 jugadores que entre ellos crearán una
historia basada en los Mitos de Cthulhu de H.P. Lovecraft.


## Preparación de la partida

Para jugar a Untold Horrors los jugadores deben elegir a un jugador que será
nombrado jugador principal, y una vez elegido, siempre se seguirán las
siguientes reglas en caso de desacuerdos:

1. En caso de tener que elegir entre dos situaciones, siempre se elegirá la
   que menos beneficie a los personajes.
2. En caso de bloqueo o desacuerdo irresoluble durante la partida, el jugador
   principal tiene la última palabra y decidirá cómo resolver el bloqueo y
   continuar con la historia.

Esto es así porque pueden ocurrir situaciones en las que no todos los jugadores
estén de acuerdo con algún matiz de la historia, o con la elección de una
habilidad para una confrontación y otros detalles que podrían bloquear el curso
normal de la partida. De esta manera, el jugador principal puede decidir cómo
desbloquear esa situación y todos los jugadores acatarán su decisión.


## Época y lugar

Lo siguiente será ponerse de acuerdo en la época en la que se desarrollará la
historia. Puede ser en los años 1920-1930, que fue la época en la que se
desarrollaban las historias escritas por el Maestro de Providence, o, por
ejemplo, en un futuro postapocalíptico en el año 2120.

Lo importante, es que los jugadores conozcan algo la época, o se vean capaces
de imaginar esa época ficticia.

El lugar también queda a elección de los jugadores. Puede ser un lugar conocido
o desconocido, y la aventura desarrollarse en un lugar bastante amplio como una
ciudad entera, transcurrir en distintos lugares a la vez, en el mundo onírico,
o incluso ser un lugar tan pequeño como una buhardilla de una casa victoriana
cerca de Salem o Arkham.

Una vez elegidos una época y un lugar, es necesario crear los personajes para
la partida.


## Personajes

Los personajes son la parte central de la historia, pues serán el ladrillo
principal sobre el que se construirá la cadena de acontecimientos durante la
partida.

Cada personaje es "controlado" por un jugador. Un mismo jugador puede controlar
varios personajes si fuera necesario o si se desea crear una historia con mayor
variedad de personajes. No se recomienda tener más de 4 personajes a la vez,
pues puede complicar enormemente la creación de escenarios coherentes al tener
que casar todas las piezas.


### Nombre y detalles del personaje

Lo primero es elegir el nombre, el sexo y la edad del jugador. Se puede ser un
poco más concreto y añadir datos como su estado civil, el aspecto (color de
ojos, rasgos faciales prominentes como barba, aspecto y constitución física,
atuendos y forma de vestir...), carácter del personaje y otros rasgos que se
considere oportunos. Todo dependerá del nivel de detalle que queramos o
podamos aportar.

Es recomendable que estos detalles se completen también a medida que se avanza
en el proceso de creación del personaje, pues puede haber rasgos que afecten a
su aspecto. Por ejemplo, un estibador de los muelles necesita una fuerte
complexión física, y no sería muy coherente que nuestro personaje fuera débil y
enfermizo.

Cuanta más información, más detallados pueden ser los textos narrativos creados
durante la partida, pues podemos usar esos detalles para complementar partes de
la historia, pero tampoco hace falta describir hasta el más ínfimo detalle.

### Arquetipos y profesiones

Una vez tenemos los datos básicos de nuestro personaje hay que elegir qué tipo
de profesión, afición o simplemente ocupación es la que ejerce habitualmente.

Esto añade aún más detalle al personaje, dando una idea de las habilidades que
podría tener así como sus competencias tanto físicas como intelectuales.

De nuevo, el nivel de detalle queda a discreción de los jugadores, pero podría
ser interesante desarrollar las razones que le llevaron a ejercer dicha
profesión, lugar dónde estudió para ello, compañeros de universidad, lugar
actual de trabajo, etc.

Ejemplos de arquetipos usuales en las historias de Lovecraft son:

* Arqueólogo
* Vidente
* Investigador privado
* Diletante
* Profesor
* Artista
* Anticuario

Se recomienda que los personajes que vayan a participar formen un grupo lo más
heterogéneo posible ya que ayudará a tener una mayor variedad de situaciones
salvables por los personajes.

### Características

Las características son rasgos que definen lo bueno que es un personaje en
determinadas áreas o situaciones. Son lo suficientemente genéricas como para
poder usarlas para determinar si el jugador es capaz de realizar y o resolver
exitosamente una situación concreta.

Hay cuatro características distintas:

* **Físico** : Habilidades y condición física y atlética.
* **Intelecto** : Conjunto de habilidades del conocimiento, lógica y deducción
* **Habilidad** : Habilidades manuales, manejo de máquinas y vehículos
* **Social** : Capacidades comunicativas y de convicción, protocolo.

Basándonos en la profesión de nuestro personaje deberemos asignar 3, 2, 1 y 1
punto respectivamente a las características.

### Comprobaciones

Durante la historia, los jugadores se verán en la necesidad de poner a prueba
sus habilidades para resolver situaciones de todo tipo. Pueden ser tareas
simples como encontrar un manuscrito en una biblioteca, o atacar a un sectario
que le cierra el paso a la puerta de salida.

Sea como sea, dichas situaciones se resuelven de la siguiente manera:

1. Se lanzará un número de dados de 6 igual al total de la característica
   requerida por la confrontación.
2. El resultado es exitoso si cualquiera de los dados lanzados es un 5+.

## Estructura de la partida

La partida se desarrolla como una sucesión de pequeños fragmentos encadenados
que terminarán formando la historia al completo. Cada relato es creado por los
jugadores en sus turnos, pero se discuten de manera conjunta para ayudar a la
coherencia general de la historia.

### Fase de planteamiento

La partida comienza siempre con una introducción de los personajes. Debemos
describir su localización y su situación inicial para poder crear un punto de
partida para la historia. No es necesario que los personajes se conozcan entre
sí. Puede que eso ocurra al avanzar los hechos.

### Fase de desencadenante

En este punto, es cuando los jugadores tienen que describir un desencadenante.
El desencadenante es un suceso que hará que los personajes decidan investigar
los acontecimientos y circunstancias del mismo.

Un desencadenante debe ser lo suficientemente abierto como para permitir crear
una historia a su alrededor, y se recomienda que sea lo más misterioso posible.

Hay que describir el desencadenante sin pensar en las razones por las que
ocurre. Los detalles se irán desvelando a medida que la historia avance.

Algunos ejemplos de desencadenantes son:

* Muerte o herencia de un familiar
* Sueños o visiones extrañas
* Historia mitológica encontrada en un libro
* Pieza arqueológica encontrada en un yacimiento
* Hechos extraños en un museo con una nueva colección de objetos antiguos
* Expedición en búsqueda y descubrimiento de lugares remotos y antiguos
* Hallazgo de una anomalía en la genealogía de un personaje
* Avistamiento de fisuras en el tejido del espacio y el tiempo
* Viaje a través de portales a otros mundos
* Carta de un amigo, familiar o conocido pidiendo ayuda
* Visita de un personaje extraño que incita a investigar algo
* Desapariciones extrañas
* Avistamientos de criaturas monstruosas
* Eventos relacionados con rituales y sociedades secretas
* Sucesos paranormales
* Situaciones meteorológicas atípicas
* Desastres naturales
* Un hecho intrigante durante una fiesta o un viaje

Este desencadenante propiciará que alguno de los personajes se sienta inclinado
a investigar sobre un hecho o una situación concreta, y se pasará a la fase de
investigación.

> Ejemplo:
> 
> Roger Thomas recibe una carta de su amigo de la universidad James Jefferson
> en la que le comenta muy excitado que va a embarcar en una expedición cuyo
> objetivo es explorar el pacífico en busca de una serie de islas extrañas que
> algunos marineros han descrito y que sin embargo no aparecen en las
> cartas de navegación.
> 
> Su amigo le pide que vaya con él para compartir esta aventura y que está
> seguro de que habrá descubrimientos muy interesantes que cambiarán la
> concepción de la historia que tenemos hasta ahora.
>
> Como antropólogo y científico, Roger no puede negar que es una oferta
> muy tentadora, y dado que confía plenamente en el juicio de su amigo
> decide acompañarle en su larga travesía.


### Fase de Investigación

La investigación consta de tres bloques en los que los jugadores inician,
avanzan y concluyen la investigación.

Cada uno de los bloques se compone de dos turnos de acción en los que cada uno
de los personajes declara una acción por turno y relata de forma breve sus
motivaciones y resultados de dicha acción.

Cuando todos los personajes hayan realizado sus acciones tendrá lugar un turno
de confrontación que determinará el avance exitoso de la investigación.

#### Turnos de acción

Cada turno de acción permite a los personajes mejorar sus habilidades, obtener
equipo que pueda ser necesario para la investigación, obtener información de
diversas fuentes, etc, que ayudarán a avanzar la investigación y, en última
instancia, a resolverla.

A continuación de detallan los distintos tipos de acciones y sus efectos sobre
la historia y los jugadores

* **Acción de mejora**: Permiten a un personaje añadir un punto a cualquiera de
  sus características.

    > Ejemplo:
    >
    > Wilbur Whateley decide que va a utilizar una de sus acciones en estudiar
    > un libro llamado Necronomicón. Para ello se desplaza a la universidad
    > Miskatonic y solicita el acceso a la biblioteca de volúmenes raros.
    > Después de discutir con el bibliotecario durante unos minutos y al ver
    > la determinación de Whateley en leer ese libro decide permitirle el
    > acceso a la biblioteca y dedica el resto de la noche a leer el libro.
    >
    > Esto hace que Wilbur tenga un conocimiento más amplio sobre artes oscuras
    > que influye directamente en su puntuación de **Intelecto**,
    > incrementándola en un punto que podrá usar como un dado extra en las
    > comprobaciones futuras de esa característica.

* **Acción de relación**: Establecer un vínculo de relación con cualquiera de
  los demás personajes permite que en el caso de una comprobación individual,
  el jugador pueda elegir el valor más alto de entre él mismo y los jugadores
  con los que tenga un vínculo como si formaran un grupo. Esta acción requiere
  que ambos jugadores estén de acuerdo en gastar un turno cada uno para que
  tenga lugar la acción. La descripción de la acción puede ser tan simple como
  un encuentro casual en que los personajes intercambian sus tarjetas después
  de hablar de algún aspecto de la investigación, como una experiencia que les
  haya marcado a ambos por una situación dramática o dolorosa.

    > Ejemplo:
    >
    > Norma Chambers, una detective de Boston, decide que necesita a alguien
    > experto en historia antigua, pues la investigación que lleva ahora ha
    > sacado a la luz algunos grabados que parecen ser de civilizaciones
    > anteriores a los Aztecas que deben ser traducidos para obtener pistas sobre
    > el caso.
    >
    > Para ello decide ir al museo de historia de Boston y concertar una
    > cita con el profesor Charles Hendricks, experto en historia precolombina.
    >
    > Durante su encuentro, Norma muestra algunos de los grabados a Hendricks,
    > el cual queda absolutamente asombrado de lo bien conservados y de lo
    > antiguos que son.
    >
    > Hendricks asegura a Norma que que puede contar con él para ayudarle en
    > las tareas de traducción a cambio de la titularidad de los estudios
    > que publicará sobre las piezas grabadas.
    >
    > Norma acepta el trato y al dia siguiente le lleva el resto de grabados
    > para que los traduzca.
    

* **Acción de concentración**: Permite a uno de los jugadores prepararse para
  la próxima confrontación. De esta forma, puede elegir una de las tiradas de
  tipo individual de la confrontación en la que los resultados de éxito serán
  de 4+ en vez de 5+ como es habitual. Deberá elegir en qué tirada utilizar la
  concentración justo antes de realizarla. Esta preparación sólo sirve para la
  confrontación del capítulo en curso y no puede mantenerse para siguientes
  capítulos. 

    > Ejemplo:
    >
    > Alexander Gibbons sabe muy bien que si planean internarse en las cavernas
    > de las afueras de Arkham a interrumpir el ritual que los sectarios llevan
    > preparando durante los últimos días, deberá estar preparado para encontrar
    > la forma de detener los cánticos y salmos, pero más que eso, deberá ser
    > capaz de evitar que los horrores que verá allí quiebren su voluntad y le
    > induzcan a la locura. Por ello decide retirarse al hotel más temprano de lo
    > habitual y realizar unos ejercicios de meditación para serenarse y estar
    > preparado para el momento decisivo.

* **Acción de investigación general**: Permite obtener información de distintas
  fuentes u objetos que permitirán a los jugadores superar un evento de
  confrontación del tipo **Requisito general**. Los requisitos generales sólo
  servirán para la confrontación en curso y no se podrán usar en los siguientes
  capítulos.

    > Ejemplo:
    >
    > Amanda Mason decide investigar más sobre la aparición que presenció la
    > semana pasada en la casa de la bruja, así que decide ir a la biblioteca
    > de la Universidad Miskatonic para buscar más información sobre el lugar.
    >
    > Después de varias horas revisando periódicos antiguos y cartas de épocas
    > más antiguas, descubre que en ese lugar fueron asesinadas brutalmente dos
    > jóvenes muchachas en lo que creían que fueron rituales de brujería
    > perpetrados por dos brujas venidas desde Salem.
    >
    > También descubre una ilustración en la que se puede ver claramente un
    > símbolo pintado en el suelo de la habitación donde se ejecutó el ritual.
    >
    > Decide buscar el símbolo en libros antiguos sobre rituales esotéricos y
    > runas antiguas, y descubre que el símbolo es en realidad una especie de
    > círculo de protección que debía ser dibujado en el suelo con sal y cenizas.
    > Decide memorizar el símbolo y hacerse con un saquito de esos materiales
    > *"por lo que pueda pasar"*.

* **Acción de investigación específica**: Permite obtener información muy
  importante u objetos raros y reliquias que permitirán a los jugadores superar
  un evento de confrontación del tipo **Requisito específico**. Esta acción
  requiere el uso de dos turnos dada la dificultad de obtener estos elementos
  importantes. Al contrario que los requisitos generales, los requisitos
  específicos si pueden utilizarse en capítulos siguientes si no se han usado
  una vez obtenidos. Se recomienda dedicar algo más de tiempo a la parte
  narrativa en este tipo de acciones.

    > Ejemplo:
    >
    > El códice del culto de la Daga Roja indica que existe una reliquia muy
    > antigua que usaban los sumos sacerdotes del culto para derramar la sangre
    > de las víctimas de los sacrificios y convertirla en una especie de elixir
    > que les permitiría comunicarse con los Dioses Exteriores y despertarlos
    > de nuevo a la existencia.
    >
    > Rudolph Hindergard sabe muy bien que eso acarrearía el fin de la vida en
    > la tierra tal y como la conocemos, y decide que debe encontrar dicha
    > reliquia antes de que el culto la consiga. Decide gastar sus dos acciones
    > para investigar sobre el aspecto y el paradero de dicha reliquia, y al
    > parecer, es un cáliz de oro macizo con forma de caracola marina que se
    > encuentra en la colección privada de una familia prominente de Innsmouth.
    > La familia Marsh.
    >
    > Decide ir a Innsmouth a hacer una oferta al dueño de la colección, un tal
    > Barnabas Marsh, el cual tiene un aspecto enfermizo, pero al parecer, el
    > tal Barnabas no está interesado en nada que le pueda ofrecer, así que por
    > el bien de la humanidad, y aunque el pueblo es escalofriante, Rudolph
    > decide que pasará la noche allí para robar el cáliz de madrugada.
    > Después de algunas horas escalofriantes en el cochambroso hotel, Rudolph
    > baja por las escaleras de emergencia que llevan al callejón y se dirige
    > a la casa de los Marsh. La casa parece vacía y consigue introducirse en
    > ella y robar el cáliz sin ser descubierto.
    > Para no despertar sospechas huye a pie siguiendo las vías del tren que
    > salen del pueblo para no regresar jamás allí.

Si el jugador desea realizar una acción que no haya sido descrita en este 
manual, los jugadores pueden ponerse de acuerdo en lo procedente de dicha
acción en cuanto a la historia y los efectos que tendrá en la misma y en la
siguiente confrontación. Ante cualquier duda, el jugador principal sigue
teniendo la última palabra en este aspecto.


#### Turno de confrontación

El turno de confrontación obliga a resolver una serie de tiradas que cambiarán
el curso de la narrativa en una dirección u otra dependiendo del éxito de las
mismas.

Una confrontación se resuelve de manera exitosa cuando se superan las tres
tiradas de comprobación o requisitos que la conforman. 

Los distintos tipos de tiradas y requisitos que pueden tener que resolverse
son:

* **Característica de grupo**: El grupo deberá superar con éxito un desafío de
  una característica elegida al azar. Se toma como referencia el valor más
  alto de entre todos los jugadores del grupo.
* **Característica individual**: Se elegirá al azar uno de los jugadores el
  cual deberá superar con éxito un desafío de una característica elegida al
  azar.
* **Requisito general**: Para superar esta comprobación el grupo deberá haber
  obtenido un objeto o una información que permitirá continuar con la
  investigación. Valdrá también utilizar un requisito de tipo específico.
* **Requisito específico**: Funciona igual que la comprobación de requisito
  general, excepto que para superarla, el objeto o información obtenido tiene
  que ser de tipo específico.

Para determinar la composición de la confrontación, se lanzarán tres dados de
seis caras y sus resultados se consultarán en orden con la siguiente tabla en
la que se especifica el tipo de tirada o requisito según la confrontación en la
que se encuentren los personajes.


| 1D6 | Primera confrontación      | Segunda confrontación      | Tercera confrontación      |
|-----|----------------------------|----------------------------|----------------------------|
|  1  | Característica de grupo    | Característica de grupo    | Característica de grupo    |
|  2  | Característica de grupo    | Característica de grupo    | Característica individual  |
|  3  | Característica de grupo    | Característica individual  | Característica individual  |
|  4  | Característica individual  | Característica individual  | Requisito general          |
|  5  | Característica individual  | Requisito general          | Requisito general          |
|  6  | Requisito general          | Requisito específico       | Requisito específico       |

Para cada comprobación de característica, los jugadores determinarán de forma
aleatoria la característica que habrá que comprobar.

   > Ejemplo:
   >
   > Los jugadores ya han ejecutado sus acciones durante el inicio de la
   > investigación y llega el momento de enfrentarse a la primera confrontación
   > que les pondrá a prueba, así que se disponen a definir los requisitos que
   > tendrán que superar.
   >
   > Uno de los jugadores lanza los dados y obtiene un 1, un 4 y un 6 en ese
   > orden. Esto significa que la confrontación estará formada por:
   > * Una comprobación de característica de grupo
   > * Una comprobación de característica individual
   > * Un requisito general
   >
   > Para la comprobación de característica de grupo se determinas que será de
   > tipo **Social**, y la característica individual será **Física**.
   > 
   > Deciden que la historia continuará de la siguiente manera:
   > * El grupo se decide a interrumpir el ritual de sacrificio que va a tener
   >   lugar esta noche en un claro del bosque del ahorcado, así que deciden
   >   que la mejor manera es infiltrarse en el ritual disfrazándose de
   >   cultistas. (Comprobación de característica de grupo **Social**)
   > * A continuación, uno de los personajes se lanza contra el sacerdote en el
   >   momento en el que el ritual está a punto de finalizar evitando así que
   >   invoque a un ser horrible a través de un portal dimensional
   >   (comprobación de **Físico** individual de ese personaje)
   > * Finalmente, mientras se dispersan los demás cultistas uno de los
   >   personajes usa un símbolo de poder que había memorizado para poder
   >   cerrar el portal y poner fin al ritual.
   >
   > Sin embargo no todo sale como los jugadores esperan. Consiguen superar la
   > primera comprobación, de manera que consiguen infiltrarse, pero en el
   > momento de interrumpir al sacerdote, fallan la tirada de físico y deciden
   > que el jugador que iba a atacarle se ve reducido por los cultistas antes
   > de alcanzarle, provocando que sus compañeros tengan que ayudarle para que
   > no le maten.
   >
   > En la confusión, a través del portal, se cuela un monstruo
   > horrible que empieza a devorar a los cultistas y los personajes huyen
   > despavoridos del lugar.
   >
   > Probablemente tendrán que enfrentarse al monstruo
   > en la siguiente confrontación.

### Fase de epílogo

Al final de la partida, se describen las consecuencias de las decisiones que se
tomaron y los hechos acontecidos durante el transcurso de la historia creada
por los jugadores.

Dependiendo del nivel de detalle que queramos, se puede comenzar por describir
la situación en la que quedan los personajes, y continuar con el efecto de la
investigación y sus resultados, catastróficos o no, sobre el entorno local y/o
global.

El resultado del epílogo vendrá definido por el total de confrontaciones en las
que los investigadores hayan tenido éxito:

* **Ninguna**: Todo sale estrepitosamente mal. El resultado afecta gravemente
  a los jugadores y se desatan catástrofes globales
* **Sólo una**: La investigación no llega a término y la catástrofe ocurre de
  forma parcial. El resultado afecta moderadamente a los jugadores y al entorno
  local y/o global
* **Dos**: La investigación no se completa pero consiguen contenerse los daños.
  Los jugadores se ven afectados ligeramente por los acontecimientos y no hay
  consecuencias catastróficas graves que afectan únicamente al entorno local.
* **Las tres**: Los investigadores completan la investigación y evitan lo
  inevitable acabando incluso con la fuente del problema en el proceso.

Evidentemente, esto son reglas generales para el resultado. Queda a la
discreción de los jugadores el alcance y los efectos para que la historia tenga
sentido así como para generar dramatismo.


## Agradecimientos

Gracias a Esteban Manchado por las correcciones y por haber creado el juego
[Deeds not Words](http://hardcorenarrativist.org/deedsnotwords/), el
juego que inspiró la creación de Untold Horrors.